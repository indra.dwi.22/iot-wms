#!/usr/bin/python3
from pigpio_dht import DHT11
import time
import requests

requests.packages.urllib3.disable_warnings()

# server configuration
server_url = "https://wms.bulog.co.id"
database = "beras_erp"
api_key = "eyJzZXJ2aWNlIjoic3RhZ2luZy1hcGlodWIiLCJhdXRob3IiOiJydXBpbm96In0="
#(untuk di isi di sensor_code) untuk mengetahui serial number device dengan perintah:
#cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2
sensor_code = "s1"
sleep_time = 180 #every 180 second

# select db first
# url_db = server_url + "/web?db=" + database
# session = requests.Session()
#select = session.get(url_db, data={}, verify=False)

# sensor configuration
dht_sensor = DHT11(4)
url_api = server_url + "/sensor/receive"

while True:
	try:
		dht_device = dht_sensor.read()
		
		if dht_device['valid']:
			temperature = dht_device['temp_c']
			humidity = dht_device['humidity']
			
			# post data to odoo api
			data_post = {
				"sensor_code" : sensor_code,
				"temperature" : temperature,
				"humidity" : humidity
			}

			headers = {
				"Api-Key" : api_key
			}

			result = requests.post(url_api, data=data_post, headers=headers, verify=False)
					
		time.sleep(sleep_time)
	except RuntimeError as e:
		print(e)
		time.sleep(sleep_time)
	except Exception as exc:
		print(exc)
		time.sleep(sleep_time)

