#!/usr/bin/python3
from pigpio_dht import DHT11
import time
import requests

requests.packages.urllib3.disable_warnings()

sleep_time = 2 #every 180 second
# sensor configuration
dht_sensor = DHT11(4)

while True:
        try:
                dht_device = dht_sensor.read()

                if dht_device['valid']:
                        temperature = dht_device['temp_c']
                        humidity = dht_device['humidity']

                        # post data to odoo api
                        data_post = {
                                "temperature" : temperature,
                                "humidity" : humidity
                        }

                        print(data_post)


                time.sleep(sleep_time)
        except RuntimeError as e:
                print(e)
                time.sleep(sleep_time)
        except Exception as exc:
                print(exc)
                time.sleep(sleep_time)
