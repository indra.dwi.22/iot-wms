#!/usr/bin/python3
import busio
import board
import adafruit_am2320
import time
import requests

sleep_time = 180 #every 180 second

# sensor configuration

# create the I2C shared bus
i2c = busio.I2C(board.SCL,board.SDA)  # uses board.SCL and board.SDA
# i2c = board.STEMMA_I2C()  # For using the built-in STEMMA QT connector on a microcontroller
am = adafruit_am2320.AM2320(i2c)


while True:
	try:	
		# post data to odoo api
		data_post = {
			"temperature" : am.temperature,
			"humidity" : am.relative_humidity
		}

	
		print(data_post)
		time.sleep(sleep_time)
	except RuntimeError as e:
		print(e)
		time.sleep(sleep_time)
	except Exception as exc:
		print(exc)
		time.sleep(sleep_time)

