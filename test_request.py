import requests
requests.packages.urllib3.disable_warnings()

server_url = "https://test-beras.bulog.co.id"
database = "beras_staging_wms"
api_key = "eyJzZXJ2aWNlIjoic3RhZ2luZy1hcGlodWIiLCJhdXRob3IiOiJydXBpbm96In0="
sensor_code = "divti11"

url_api = server_url + "/sensor/receive"

data_post = {
             "sensor_code" : sensor_code,
             "temperature" : 23,
             "humidity" : 59
            }

headers = {
                    "Api-Key" : api_key
          }

result = requests.request("POST",url_api, data=data_post, headers=headers, verify=False)
print(result)
