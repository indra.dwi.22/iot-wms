#!/usr/bin/python3
import busio
import board
import adafruit_am2320
import time
import requests

requests.packages.urllib3.disable_warnings()

# server configuration
server_url = "https://wms.bulog.co.id"
database = "beras_erp"
api_key = "eyJzZXJ2aWNlIjoic3RhZ2luZy1hcGlodWIiLCJhdXRob3IiOiJydXBpbm96In0="
sensor_code = "s1"
sleep_time = 180 #every 180 second

# select db first
# url_db = server_url + "/web?db=" + database
# session = requests.Session()
#select = session.get(url_db, data={}, verify=False)

# sensor configuration

# create the I2C shared bus
i2c = busio.I2C(board.SCL,board.SDA)  # uses board.SCL and board.SDA
# i2c = board.STEMMA_I2C()  # For using the built-in STEMMA QT connector on a microcontroller
am = adafruit_am2320.AM2320(i2c)

url_api = server_url + "/sensor/receive"

while True:
	try:	
		# post data to odoo api
		data_post = {
			"temperature" : am.temperature,
			"humidity" : am.relative_humidity
		}

		headers = {
			"Api-Key" : api_key
		}

		result = requests.post(url_api, data=data_post, headers=headers, verify=False)
				
		time.sleep(sleep_time)
	except RuntimeError as e:
		print(e)
		time.sleep(sleep_time)
	except Exception as exc:
		print(exc)
		time.sleep(sleep_time)

